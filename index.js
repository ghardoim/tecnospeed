const express = require('express')
const app = express()

app.set('PORT', process.env.PORT || 3000)
app.get('/', (req, res) => {
  res.status(200).send('<h1>Hello TecnoSpeed!</h1>')
})

app.listen(app.get('PORT'), () => console.log("Hello TecnoSpeed!"))